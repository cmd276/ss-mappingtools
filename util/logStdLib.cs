//------------------------------------------------------------------------------
//
//  Filename: Dark_Overrun.cs
//
//  AUTHOR: ^TFW^ DarkFlare
//
//------------------------------------------------------------------------------
$LOG::isEnabled = true;
$LOG::useTimeStamp = true;

dataFromString(getTime(), ":", "$log::hour", "$log::minute", "$log::seconds");
dataFromString(getDate(), "//", "$log::month", "$log::day", "$log::year");

$LOG::logFile = "tmp\\" @ $Location @ "." @ $log::year @ $log::month @ $log::day @ "-" @ @ $log::hour @ $log::minute @ $log::seconds @ ".log";


//------------------------------------------------------------------------------
//--  Mission Related.
//------------------------------------------------------------------------------
function log::setRules ()
{
  log::create("setRules();");
}

function log::setDefaultMissionOptions()
{
  log::create("setDefaultMissionOptions();");
  // Dont use this... Ever... I wont even log this stuff... 
}
function log::onMissionStart() 
{
  log::create("Mission Started.");
}
function log::onMissionLoad()
{
  %string = "//-------------------------------------------------------------------------Start\n" @
  getTimeDate() @ ": " @ "Mission ['" @ $missionName @ "'] Loading...";
  
  fileWrite($LOG::logFile, append, %string);
}
function log::onMissionEnd() 
{
  %string = "\n" @
    //strAlign(20,left,$ScoreBoard::PlayerColumnHeader1) @
    strAlign(20,left,"Name");
    %count = 2; 
    while ($ScoreBoard::PlayerColumnFunction[%count] != "") 
    { 
      %string = %string @ strAlign(10,left,$ScoreBoard::PlayerColumnHeader[%count]);
      %count++;
    }
    %string = %string @"\n"; 
  for(%i = 0; %i < playerManager::getPlayerCount(); %i++) 
  {
    %p = playerManager::getPlayerNum(%i);
    %string = %string @ %_2 = strAlign(20, left, getName(%p));
    %count = 2; 
    while ($ScoreBoard::PlayerColumnFunction[%count] != "")
    {
      %string = %string @ strAlign(10, left, eval($ScoreBoard::PlayerColumnFunction[%count] @ "(" @ %p @ ");"));
      %count++;
    }
    %string = %string @"\n"; 
  }


  %string = %string @ getTimeDate() @ ": " @ "Mission ['" @ $missionName @ "'] Finished...\n" @
  "//---------------------------------------------------------------------------End\n";

  fileWrite($LOG::logFile, append, %string);
}

//------------------------------------------------------------------------------
//--  Player Related.
//------------------------------------------------------------------------------
function log::player::onAdd(%player) 
{
  log::create(getName(%player)@" joined the server.");
}
function log::player::onRemove(%player) 
{
  %string = "Player Left\n" @
  //strAlign(20,left,$ScoreBoard::PlayerColumnHeader1) @
  strAlign(20,left,"Name");
  %count = 2; 
  while ($ScoreBoard::PlayerColumnFunction[%count] != "") 
  { 
    %string = %string @ strAlign(10,left,$ScoreBoard::PlayerColumnHeader[%count]);
    %count++;
  }
  %string = %string @"\n"; 

  %string = %string @ %_2 = strAlign(20, left, getName(%player));
  %count = 2; 
  while ($ScoreBoard::PlayerColumnFunction[%count] != "")
  {
    %string = %string @ strAlign(10, left, eval($ScoreBoard::PlayerColumnFunction[%count] @ "(" @ %player @ ");"));
    %count++;
  }
  %string = %string @"\n"; 

  log::create(%string);
}
function log::player::onDestroyed(%destroyed, %destroyer)
{
  log::create(getName(%destroyed) @ " killed by " @ getName(%destroyer));
}

//------------------------------------------------------------------------------
//--  Vehicle Related.
//------------------------------------------------------------------------------
function log::vehicle::onAdd(%vehicleId) 
{
  log::create(getHudName(%vehicleId) @ " joined map.");
}
function log::vehicle::onDestroyed(%destroyed, %destroyer) 
{
  log::create(getHudName(%destroyed) @ " killed by " @ getHudName(%destroyer));
}
function log::vehicle::onAttacked(%attacked, %attacker)
{
  log::create(getHudName(%attacked) @ " attacked by " @ getHudName(%attacker));
}
function log::vehicle::onArrived(%this,%where)
{
  log::create(%this @ " arrived " @ %where);
}
function log::vehicle::onScan(%scanned, %scanner)
{
  log::create(getHudName(%scanned) @ " scanned by " @ getHudName(%scanner));
}
function log::vehicle::onSpot(%spotter,%target)
{
  log::create(getHudName(%target) @ " spotted by " @ getHudName(%spotter));
}
function log::vehicle::onMessage(%destroyer,%message,%target)
{
  log::create(getHudName(%destroyer)@"->"@getHudName(%target)@": "@%message);
}

//------------------------------------------------------------------------------
//--  Structure Related.
//------------------------------------------------------------------------------
function log::structure::onDestroyed(%attacked, %attacker)
{
  log::create(getHudName(%attacked) @ " destroyed by " @ getHudName(%attacker));
}
function log::structure::onScan(%scanned,%scanner)
{
  log::create(getHudName(%scanned) @ " scanned by " @ getHudName(%scanner));
}
function log::structure::onAttacked(%attacked, %attacker)
{
  log::create(getHudName(%attacked) @ " attacked by " @ getHudName(%attacker));
}

//------------------------------------------------------------------------------
//--  Repeated code made functional... 
//------------------------------------------------------------------------------

function getTimeDate()
{
  return getDate() @ " " @ getTime();
}

function log::create(%msg)
{
  //echo(%msg);
  fileWrite($LOG::logFile, append, getTimeDate() @ ": " @ %msg);
}

function vehicle::onAddLog()
{
    
}

echo("Log.cs Executed @ " @ getTimeDate());

//function createLog() {
//    //Time Variables
//    dataFromString(getTime(), ":" "$log::hour", "$log::minute", "$log::seconds");
//    //Date Variables
//    dataFromString(getDate(), "//", "$log::month", "$log::day", "$log::year");
//
//    %fileName = $log::year @ $log::month @ $log::day @ "-" @ $log::hour @ $log::minute @ "-etcetcetc";
//}

//------------------------------------------------------------------------------
// -- Below code stolen from: :e: Drakeâ„¢
// -- From the project: stringManager.cs
//------------------------------------------------------------------------------

function dataFromString(%string, %delimeter, %var0, %var1, %var2, %var3, %var4, %var5, %var6, %var7, %var8, %var9, %var10, %var11, %var12) {
	if(%var0 == "")
		return;

	%varCount = 0;
	%totalVars = 12;
	while(eval("return %var" @ %varCount @ ";") != "") {
		if(%varCount > %totalVars)
			continue;
		else
			%varCount++;
	}

	for(%i = 0; %i < %varCount; %i++) {
		if(%i < %varCount - 1) {
			%pos = 1;
			%strShort = strAlign(%pos, left, %string);
			%last = "";
			while(%strShort != %last @ %delimeter) {
				%last = %strShort;
				%strShort = strAlign(%pos, left, %string);
				%pos++;
			}
			eval(eval("return %var" @ %i @ ";") @ " = \"" @ %last @ "\";");
		} else
			eval(eval("return %var" @ %i @ ";") @ " = \"" @ %string @ "\";");
		%string = strAlignR(strLen(%string) - %pos + 1, %string);
	}
}