// FILENAME:    circleStdLib3.cs
//
// AUTHORS:     ^TFW^ Wilzuun
// START DATE:  Oct 10 2018
// LAST MOD:    Oct 27 2018
// VERSION:     3.0
//
##-------------------------------------------------------------------------------------------------------------------------------
//  --  Version History
//
//  3.0
//      Added in options for XZ, YZ plane circles.
//      Added functions:
//          setPlane()
//
//  2.4
//      Better documentation of variable names and usage.
//      Added in feature to make all items spawn at the same height.
//      Added in Center of circle marker option
//
//  2.3
//      -What did I do?-
//
//  2.2
//      Organized functions and variables to be in alphabetical order.
//
//  2.1
//      Added few safe gaurds to make sure objects actually spawn.
//      Fixed seemingly random unit from existing by deleting it. (SetObject now deletes the object handed to it)
//
//  2.0
//      Created namespace `Circle`
//      Rewrote great majority of code to fit into new namespace.
//      Expanded functions.
//          Created Cleanup()
//          Created Init()
//          Created SetCount()
//          Created SetLocation()
//          Created SetMode()
//          Created SetObject()
//          Created SetSize()
//          Renamed `makeCircle()` to `SpawnCircle()`
//
//  1.5
//      Added Mode types, and correlating formulas
//
//  1.0
//      Basic circle creation.
//      Added Offsets.
//      Added custome objects.
//
##-------------------------------------------------------------------------------------------------------------------------------
// -- NOTES:
//  This file isnt meant to be edited.
//  This file relies on Trigonometry Standard Library (TrigonometryStdLib.cs) by Com. Sentinal [M.I.B.]
##-------------------------------------------------------------------------------------------------------------------------------
//  $circle::count : int
//      This variable is set internally by some logic. It dictates how many
//      items are currently spawned.
$circle::count       = 0;

//  $circle::mode : str
//      Available options:
//          "angle"    : How many degrees around the circle items should be placed.
//          "count"    : A flat number of how many items to spawn.
//          "distance" : How many meters between the items.
//      Please run setMode() for verification checks.
$circle::mode        = false;

//  $circle::orbs[0] : objectArray
//      An internally managed array of objects used in the creation of the circle.
//      Used in deleting and creating items on circle perimeter.
$circle::orbs[-1]    = true;

//  $circle::radius : int
//      User supplied size of the circle that will be spawned.
//      POSITIVE INTEGERS ONLY!
$circle::radius      = false;

//  $circle::spawnCount : int
//      A count of how many items should be spawned. Both internally and externally spawned.
$circle::spawnCount  = false;

//  $circle::xOffset : int
//  $circle::yOffset : int
//  $circle::zOffset : int
//      Respectively causes an offset from 0,0,0 location. These numbers can be postive, or negative.
//      they can also be floating point numbers as well.
//      Notes on zOffset : This will adjust items based on where the terrain is. So a 0 is *at* terrain height,
//      a positive number will cause the item to float, a negative number will cause it to be underground.
//  $circle::zCenter : boolean
//      True  : Use the center of the circle's terrain height as the base height.
//      False : Use the item's 'drop' point's terrain height as the base height.
//      Using this will allow for the circle objects to appear at the same height on hilly maps.
$circle::xOffset     = 0;
$circle::yOffset     = 0;
$circle::zOffset     = 0;
$circle::zCenter     = true;

//  $circle::zRotate : boolean
//  $circle::xRotate : boolean
//      Should an item be rotated?
//      These are not interconnected.
//      zRotate will make the item face center.
$circle::zRotate     = false;
$circle::xRotate     = false;

//  $circle::zRotMod : int
//  $circle::xRotMod : int
//      Respective modifiers for rotations. These are in degrees.
//      xRotMod @ 90 will cause the item to stand vertically (as if placed on a wall)
//              @ 270 will result in the same thing as 90.
//              @ 180 will cause items to be upside down.
//      zRotate spins the item.
//      xRotate tilts the item.
$circle::zRotMod     = 0;
$circle::xRotMod     = 90;


//  $circle::markCenter : bool
//      Mark the center of the circle with a marker?
$circle::markCenter  = true;
//$circle::marker     = "";


//  $circle::plane : str
//      Which plane are we building on?
//          "XY" : Parallel with / On the ground
//          "YZ" : Faces East / West. Appears as a line from North/South views.
//          "XZ" : Faces North/South. Appears as a line from East / West views.
$circle::plane       = "xy";

//  $circle::underground : bool
//      Force rendering of underground objects.
$circle::underground = false;

##-------------------------------------------------------------------------------------------------------------------------------
// -- Circle::Cleanup ()
// Deletes all of the remaining perimeter objects. Then resets the count to zero.
// Make sure you use it in onMissionEnd()
function Circle::Cleanup()
{
    echo("Circle::Cleanup()");
    # Attempt to delete existing perimeter objects.
    for(%item=0;%item<=$circle::count;%item++)
    {
        deleteObject($circle::orbs[%item]);
    }
    // reset count, so that future deletes are faster! (or something like that)
    $circle::count = 0;
}

// -- Circle::Init ()
//  %radius         int         Size of the ring.
//  %xOffset        int         Grid offset on the X Axis.
//  %yOffset        int         Grid offset on the Y Axis.
//  %mode           str         How to measure distance between the 'orbs' or objects on the circle's perimeter: options: `angle`, `distance`, `count`
//  %orbNumber      int         The measure or volume of items for perimeter.
//  %object         ObjId       The object to put on the circle's perimeter.
//  Does the heavy work of setting everything up for the player.
function Circle::Init (%size, %xOffset, %yOffset, %mode, %measure, %object)
{
    echo("Circle::Init()");
    %fail = false;
    //do fail safe testing, these params are required.
    if (%size == "") %fail = true;
    if (%xOffset == "") %fail = true;
    if (%yOffset == "") %fail = true;
    if (%mode == "") %fail = true;
    if (%measure == "") %fail = true;

    if (%fail == true)
    {
        echo("Cannot complete actions. Check your params: <size>, <xPosition>, <yPosition>, <count|angle|distance>, <measure>, [objectToUse]");
        return 'err';
    }

    Circle::SetMode(%mode, %measure); // MUST BE FIRST. EVEN IN MANUAL CALLS.
    if ($circle::mode != false) // we have a proper mode set, proceed with setup.
    {
        Circle::SetLocation(%xOffset, %yOffset); // where we center the circle.
        Circle::SetObject(%object);  // store the boject we'll be using.
        Circle::SetSize(%size); // set the radius
        schedule("Circle::SpawnCircle();",2); // MUST BE LAST
    }
}

// -- Circle::SetCount ()
//  %measure        int         The measure or volume of items for perimeter.
//  Used when you want to change how many items there are, but not the mode used.
function Circle::SetCount(%measure)
{
    Circle::SetMode($circle::mode, %measure); // it was secretly a redirect...
}

// -- Circle::SetLocation ()
//  %xOffset        int         Grid offset on the X Axis.
//  %yOffset        int         Grid offset on the Y Axis.
// relocation of the circle can be doen with this. Make sure you SpawnCircle() afterwards!
function Circle::SetLocation (%xOffset, %yOffset)
{
    echo("Circle::SetLocation()");
    $circle::xOffset = %xOffset; // heres a spot...
    $circle::yOffset = %yOffset; // theres a spot...
    // where is a spot spot
}

// -- Circle::SetMode ()
//  %mode           str         How to measure distance between the 'orbs' or objects on the circle's perimeter
//  %measure        int         The measure or volume of items for perimeter.
//Sets the count of items for the perimeter, must also be called first in setup chain.
function Circle::SetMode(%mode, %measure)
{
    echo("Circle::SetMode()");
    // Variuos math for varius modes.
    if (%mode == "angle")
    {
        $circle::spawnCount = 360/%measure;
    }
    else if (%mode == "distance")
    {
        $circle::spawnCount = (pi() * (%radius * 2))/%measure;
    }
    else if (%mode == "count")
    {
        $circle::spawnCount = %measure;
    }
    else
    {
        // no proper mode? No setting anything!
        echo ( "Unable to process request, %mode is invalid. Must be 'angle', 'distance', or 'count'" ) ;
        return 'err';
    }

    $circle::mode = %mode;
    $circle::spawnCount = abs($circle::spawnCount); // a negative can cause a lot of issues... lets make it positive.
}

// -- Circle::SetObject ()
//  %object         ObjId       The object to put on the circle's perimeter.
//  Change the object used for circle perimeter. Make sure to SpawnCircle() afterwards.
function Circle::SetObject (%object)
{
    echo("Circle::SetObject()");
    if (%object == "") // Fi they dont provide a vaild object, give em Turry, the Turret.
    {
        echo ( "Because no object was given, one has been supplied for you, expect a bill later.");
        %object = newObject("Turry The Turret", Turret, 7 );
    }

    storeObject(%object, "object_circleStdLib"); // Store the object for mass production later.
    deleteObject(%object); // delete the original object... it does weird things if you dont.
}

function Circle::SetPlane(%newPlane)
{
    if ((%newPlane == "xy") || (%newPlane == "yz") || (%newPlane == "xz") ||
        (%newPlane == "yx") || (%newPlane == "zy") || (%newPlane == "zx"))
    {
        $circle::plane = %newPlane;
    }
    else
    {
        // echo("Circle::SetPlane(): Error: Wrong plane type.");
    }
}

// -- Circle::SetSize ()
//  %radius         int         Size of the ring.
//  %math           bool
//  If %math is true, will add the %newSize to the current radius, otherwise complete override.
function Circle::SetSize (%newSize, %math)
{
    echo("Circle::SetSize()");
    if (%math == true) // we want to add it to the current radius.
        $circle::radius = abs($circle::radius + %newSize);
    else // we want an override.
        $circle::radius = abs(%newSize);

    // We'll recalculate the amount of 'orbs' we need.
    // Mainly due to if they selected `distance` and had 500m ring, at every 50m an 'orb'
    // And drop that to 250m ring, We should go from 10 orbs, to 5 orbs. Otherwise they aren't 50m apart any more.
    Circle::SetMode($circle::mode, $circle::spawnCount);

}

// -- Circle::SpawnCircle ()
//
//  Must be called last in the setup sequence.
//  Does the actual placement of the Circle.
function Circle::SpawnCircle ()
{
    Circle::Cleanup();
    %itemCount = $circle::spawnCount;
    if ($circle::markCenter == true)
    {
        $circle::marker = loadObject(%object, "object_circleStdLib");
        %xPos = $circle::xOffset;
        %yPos = $circle::yOffset;
        %zPos = getTerrainHeight(%xPos, %yPos) + $circle::zOffset;
        setPosition($circle::marker, %xPos, %yPos, %zPos, %zRot, %xRot);
    }
    for(%item=0;%item<=%itemCount;%item++)
    {
        %xPos = positionX(%itemCount, %item, $circle::radius) + $circle::xOffset;
        %yPos = positionY(%itemCount, %item, $circle::radius) + $circle::yOffset;
        %zPos = getTerrainHeight(%xPos, %yPos);
        if ($circle::zCenter == true)
            %zPos = getTerrainHeight($circle::xOffset, $circle::yOffset);
        
        if (($circle::plane == "yz") || ($circle::plane == "zy"))
        {
            %xPos = positionX(%itemCount, %item, $circle::radius) + $circle::xOffset;
            %yPos = $circle::yOffset;
            %zPos = getTerrainHeight(%xPos, %yPos) + positionY(%itemCount, %item, $circle::radius);
        }
        if (($circle::plane == "xz") || ($circle::plane == "zx"))
        {
            %xPos = $circle::xOffset;
            %yPos = positionY(%itemCount, %item, $circle::radius) + $circle::yOffset;
            %zPos = getTerrainHeight(%xPos, %yPos) + positionX(%itemCount, %item, $circle::radius);
        }
        %zPos = %zPos + $circle::zOffset;

        if ($circle::zRotate)
        {
            %zRot = getAngle($circle::xOffset, $circle::yOffset, %xPos, %yPos);
            if (%zRot == "") %zRot = 90;
            %zRot = %zRot + $circle::zRotMod;
        }
        else
            %zRot = 0;

        if ($circle::xRotate)
            %xRot = $circle::xRotMod;
        else
            %xRot = 0;

        if (($circle::underground == true) ||
            (($circle::underground == false) && (%zPos >= getTerrainHeight(%xPos, %yPos)))
           )
        {
            $circle::orbs[%item] = loadObject(%object, "object_circleStdLib");
            $circle::count = %item;
            setPosition($circle::orbs[%item], %xPos, %yPos, %zPos, %zRot, %xRot);
        }
    }
}

// These two functions were stolen from the internet, and adapted to fit starsiege.

function positionX(%numItems, %thisNum, %radius)
{
    %alpha = 360/%numItems; // angle between the elements
    %angle = %alpha * %thisNum; // angle for N element
    %x = %radius * cos(%angle); // X coordinates
    return %x;
}

function positionY(%numItems, %thisNum, %radius)
{
    %alpha = 360/%numItems; // angle between the elements
    %angle = %alpha * %thisNum; // angle for N element
    %y = %radius * sin(%angle); // Y coordinates
    return %y;
}