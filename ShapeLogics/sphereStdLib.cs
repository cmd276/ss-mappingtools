// FILENAME:    sphereStdLib.cs
// AUTHORS:     ^TFW^ Wilzuun
// LAST MOD:    29 Apr 2019
// VERSION:     1.0
##-------------------------------------------------------------------------------------------------------------------------------
//  --  Version History
//
//  2.0 - 29 Apr 2019
//      Started comments.
//      Added in all on-the-fly variables
//      Added in the ID functions (GetNextId() & __IdUsed(%id))
//  
//  1.0 - 28 Oct 2018
//      Started ground works.
//      Finished ground work on 29 Apr 2019.
##-------------------------------------------------------------------------------------------------------------------------------
//  --  Notes
//  The sphere script is pretty much a modified circleStdLib.cs file.
//  Utilizing a bit more math, and a bit more complexity.
//  The word "level" is used constantly throughout this file, to refer to a spawned group of items in a sphere, that are at the same height.
//      

// FOR THE DRAKE
// I removed the array, and the counter.
// In place of them, used a missionGroup.
// This may break Animations though. I unno.
##-------------------------------------------------------------------------------------------------------------------------------

// We require this library, from Sentinel of MIB.
exec("TrigonometryStdLib.cs");

// The group name when making a new sphere.
$sphere::GroupName = "TheSphere";
// Do we spawn underground? typically we'd say no.... But it gets weird if we dont.
$sphere::subterrain = true;
// this will be renamed to `underground` for consistency across files.

//  $sphere::xOffset : int
//  $sphere::yOffset : int
//  $sphere::zOffset : int
//      Respectively causes an offset from 0,0,0 location. These numbers can be postive, or negative.
//      they can also be floating point numbers as well.
//      Notes on zOffset : This will adjust items based on where the terrain is. So a 0 is *at* terrain height,
//      a positive number will cause the item to float, a negative number will cause it to be underground.
$sphere::xOffset = 0;
$sphere::yOffset = 0;
$sphere::zOffset = 0;

//  $sphere::markCenter : bool
//      Mark the center of the sphere with a marker?
$sphere::markCenter  = false;
//  $sphere::marker : object
//      If markCenter is true, uses the following object as the sphere center marker.
$sphere::marker      = "";

//  $sphere::radius : int
//      User supplied size of the sphere that will be spawned.
//      POSITIVE INTEGERS ONLY!
//  $sphere::spawnVerical : int
//      How many "levels" should be used to make the sphere?
//  $sphere::spawnHorizontal : int 
//      How many objects per "level"
$sphere::radius = 300;
$sphere::spawnVerical = 10;
$sphere::spawnHorizontal = 10;

//  $sphere::zRotate : boolean
//  $sphere::xRotate : boolean
//      Should an item be rotated?
//      These are not interconnected.
//      zRotate will make the item face center.
$sphere::xRotate = false;
$sphere::zRotate = false;

//  $sphere::zRotMod : int
//  $sphere::xRotMod : int
//      Respective modifiers for rotations. These are in degrees.
//      xRotMod @ 90 will cause the item to stand vertically (as if placed on a wall)
//              @ 270 will result in the same thing as 90. Just the other way.
//              @ 180 will cause items to be upside down.
//      zRotate spins the item.
//      xRotate tilts the item.
$sphere::xRotMod = -1;
$sphere::zRotMod = -1;
//  $sphere::underground : bool
//      Force rendering of underground objects.
$sphere::underground = true;

// Setup the things we need for the sphere.
function sphere::Init()
{
    echo("sphere::Init()");
    // Creat an object, add it to the mission group, let the sphere know we're good.
    $sphere::group = newObject("TheSphere", SimGroup);
    addToSet("MissionGroup", $sphere::group);
    $sphere::init = true;
}

function sphere::getNextId()
{
    %MissionGroup = getObjectId("MissionGroup");
    $checkId = 0;
    while(sphere::__IdUsed($checkId))
    {
        %checkId = %checkId + 1;
    }
    return %checkId;
}

function sphere::__IdUsed(%id)
{
    %group = getObjectId("MissionGroup");
    %item = getNextObject(%group, 0);
    for (%count = 0; %x = %count; %count = %count+1)
    {
        if (getObjectName(%item) == "TheSphere" @ %id)
        {
            return true;
        }
        %item = getNextObject(%group, %item);
    }
    return false;
}

function sphere::Create(%radius, %xOffset, %yOffset, %zOffset, %vSpawn, %hSpawn)
{
    // Echo out so we know we're running....
    echo("sphere::Create()");
    // Clean up any previous setups.
    sphere::Cleanup(); // old now...
    // Setup if not set up.
    if ( ! $sphere::init ) sphere::Init();

    if ($sphere::markCenter)
    {
        %item = newObject("CenterMarker",getObjectType($sphere::marker), $sphere::marker);
        addToSet($sphere::group, %item);
        setPosition(%item, %xOffset, %yOffest, %zOffset);
    }    
    // zBase is used so that all items in the sphere are properly aligned.
    %zBase = getTerrainHeight(%xOffset, %yOffset);

    // The sphere is "cut" into "levels" and we need to calculate each "level"
    for (%v = 0; %v <= %vSpawn/2; %v++)
    {
        // This calculation is for everything in the same "level"
        // Theta Angle.
        %tSin = sin((360/%vSpawn) * %v);
        %tCos = cos((360/%vSpawn) * %v);

        // The circle that has to be made per "level"
        for (%h = 0; %h <= %hSpawn; %h++)
        {
            // Sigma Angle.
            // this dictates rotation around the Z axis.
            %sCos = cos((360/%hSpawn) * %h);
            %sSin = sin((360/%hSpawn) * %h);

            // Find out placement of the item...
            %xPos = %radius * %sCos * %tSin;
            %yPos = %radius * %sSin * %tSin;
            %zPos = (%radius * %tCos) + %zBase + %zOffset;

            // get the terrain height of this exact spot...
            // Why? because if we dont spawn thigns underground, we'll know it
            // with this variable.
            %terrain = getTerrainHeight(%xPos, %yPos);

            if ($sphere::zRotate)
            {
                if ($sphere::zRotMod > -1)
                {
                    // zRot makes things rotate around the Z axis... which i always mistake.
                    %zRot = getAngle(%xPos,%yPos, %xOffset, %yOffest);
                    // if zRot == "" make it equal 90. For what ever magical reason, 
                    // 90 degrees always comes up as a blank string.
                    if (%zRot == "") %zRot = 90;
                    // add 90 degrees so that it faces inward, towards the center.
                    %zRot = %zRot + 90;
                }
                else
                {
                    $zRot = $sphere::zRotMod;
                }
            }
            // xRot changes the pitch of the object we're spawning. Its responsible
            // for making a sphere when you use bridges as the objects.
            // only set it on the first object of this "level" as the objects get
            // spawned, their angle changes slightly, causing odd behavoir.
            if ((%h < 1) && $sphere::xRotate)
            {
                // get the angle of object placement, and center of sphere.
                %xRot = getAngle(%xPos,%zPos, %xOffset,(%zBase + %zOffset));
                // for the same reason as zRot, a 90 degree is magically empty string
                // answered... wtf is with that?
                if (%xRot == "") 
                {
                    %xRot = 90;
                }
                // Make them face inward, subtract 90 degrees. 
                // Wanna make them face outward? add 90 instead!
                %xRot = %xRot + -90;
            }

            // verify that we're placing it.
            if ((%zPos >= %terrain) || ($sphere::subterrain))
            {
                // create teh object...
                %item = newObject("EMP Projectile", StaticInterior, "xbridgehub.dis");
                // add it to group...
                addToSet($sphere::group, %item);
                // "spawn" it. since all created items are spawned anyways... just below ground
                // move it to where it can be seen...
                setPosition(%item, %xPos, %yPos, %zPos, %zRot, %xRot);
            }
        }
    }
}


function sphere::Cleanup(%id)
{
    echo("sphere::Cleanup('" @ %id @ "')");
    deleteObject("TheSphere" @ %id );
}

// function sphere::Cleanup()
//{
    // cleaning up our messes... What better way to clean a virtual mess than by
    // deleting it outright?
//    echo("sphere::Cleanup()");
//    deleteObject($sphere::group);
    // we're also no longer init'd. since to be init'd means we have a group.
//    $sphere::init = false;
//}
