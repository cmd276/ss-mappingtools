function shape::getNextId(%groupName)
{
    %checkId = 0;
    while(sphere::__IdUsed(%groupName, %checkId))
    {
        %checkId = %checkId + 1;
    }
    return %checkId;
}

function shape::__IdUsed(%groupName, %id)
{
    %group = getObjectId("MissionGroup");
    %item = getNextObject(%group, 0);
    for (%count = 0; %item != 0; %count = %count+1)
    {
        if (getObjectName(%item) == %groupName @ %id)
        {
            return true;
        }
        %item = getNextObject(%group, %item);
    }
    return false;
}