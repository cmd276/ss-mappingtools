$cube::GroupBase = "TheCube";
$cube::width = 10;
$cube::height = 10;
$cube::depth = 10;
$cube::xOffset = 100;
$cube::yOffset = 100;
$cube::zOffset = 0;
$cube::underground = false;

$cube::outline = true; // if false, create sides.
$cube::unit = "distance"; // distance || count
$cube::volume = 10;

// $Cube::Count = -1;
// $Cube::objects[-1] = true;

// function Cube::Cleanup()
// {
//     echo("Cube::Cleanup()");   
//     # Attempt to delete existing perimeter objects.
//     for(%item=0;%item<=$Cube::Count;%item++)
//     {
//         deleteObject($Cube::objects[%item]);
//     }
//     // reset count, so that future deletes are faster! (or something like that)
//     $circle::count = 0;
// }

// function Cube::MakeOutlineByDimension (%height, %width, %depth, %xOffset, %yOffset, %zOffset, %distance)
// {
//     echo("Cube::MakeOutlineByDimension()");   
//     Cube::MakeOutlineByLocation(%xOffset-%width, %yOffset-%depth, %zOffset-%height, %xOffset+%width, %yOffset+%depth, %zOffset+%height, %distance);
// }

// function Cube::MakeOutlineByLocation (%x1, %y1, %z1, %x2, %y2, %z2, %distance)
// {
//     echo("Cube::MakeOutlineByLocation()");   
//     Cube::Cleanup();
//     %xCover = max(%x1,%x2) - min(%x2,%x1);
//     %xStep = %xCover / %distance;
//     %yCover = max(%y1,%y2) - min(%y2,%y1);
//     %yStep = %yCover / %distance;
//     %zCover = max(%z1,%z2) - min(%z2,%z1);
//     %zStep = %zCover / %distance;
    
//     for(%i=min(%x2,%x1); %i <= max(%x1,%x2); %i = %i + %distance)
//     {
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %i, %y1, %z1+getTerrainHeight(%x1,%y1));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %i, %y2, %z1+getTerrainHeight(%x2,%y2));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %i, %y1, %z2+getTerrainHeight(%x1,%y1));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %i, %y2, %z2+getTerrainHeight(%x2,%y2));
//     }
//     for(%i=min(%y2,%y1); %i <= max(%y1,%y2); %i = %i + %distance)
//     {
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %x1, %i, %z1+getTerrainHeight(%x1,%y1));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %x2, %i, %z1+getTerrainHeight(%x2,%y2));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %x1, %i, %z2+getTerrainHeight(%x1,%y1));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %x2, %i, %z2+getTerrainHeight(%x2,%y2));
//     }
//     for(%i=min(%z2,%z1); %i <= max(%z1,%z2); %i = %i + %distance)
//     {
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %x1, %y1, %i+getTerrainHeight(%x1,%y1));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %x1, %y2, %i+getTerrainHeight(%x1,%y2));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %x2, %y1, %i+getTerrainHeight(%x2,%y1));
//         $Cube::Count++;
//         $Cube::objects[$Cube::Count] = newObject("EMP Projectile", StaticShape, "pr_emp.dts");
//         setPosition($Cube::objects[$Cube::Count], %x2, %y2, %i+getTerrainHeight(%x2,%y2));
//     }
// }